<?php

namespace App\Console\Commands;

use App\Http\Controllers\CodeContainer;
use App\Http\Controllers\CodesGenerator;
use App\Http\Controllers\FileGenerator;
use Illuminate\Console\Command;

class CodeGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:code-generator {--quantity=} {--length=} {--file=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Codes generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     *
     * @return mixed
     */
    public function handle()
    {
        $codeContainer = new CodeContainer(
            intval($this->option('quantity')),
            intval($this->option('length'))
        );

        if (file_exists( public_path($codeContainer->getQuantity() . '_' . $codeContainer->getLength()))) {
            print_r("Codes package with given quantity and code's length already exists!");
            exit();
        }
        try {
            CodesGenerator::generate($codeContainer);
            FileGenerator::create($codeContainer, $this->option('file'));
            print_r("Codes package has been created correctly!");
            exit();
        } catch (\Exception $e) {
            var_dump($e);
            exit();
        }
    }
}
