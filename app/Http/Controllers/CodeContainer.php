<?php

namespace App\Http\Controllers;

class CodeContainer
{
    protected $quantity;

    protected $length;

    protected $data;

    public function __construct(int $quantity, int $length)
    {
        $this->quantity = $quantity;
        $this->length   = $length;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return int
     */
    public function getLength(): int
    {
        return $this->length;
    }

    /**
     * Sets array of generated codes
     *
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * Returns generated codes
     *
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }
}
