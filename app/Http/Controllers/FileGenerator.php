<?php

namespace App\Http\Controllers;

/**
 * Class FileGenerator - saves generated codes in file
 */
class FileGenerator
{
    /**
     * Saves codes in file
     *
     * @param CodeContainer $container
     * @param string|null $filename
     * @throws \Exception
     */
    public static function create(CodeContainer $container, string $filename = null)
    {
        try {
            $fileName = !is_null($filename) ? $filename : $container->getQuantity() . '_' . $container->getLength();

            $directory = $container->getQuantity() . '_' . $container->getLength();
            mkdir(public_path($directory), 0777, true );

            $fp = fopen( public_path($directory) . '/' . $fileName  . '.csv', 'w');
            foreach($container->getData() as $line){
                $val = explode(",",$line);
                fputcsv($fp, $val);
            }
            fclose($fp);
        } catch (\Exception $e) {
            throw new \Exception('There were problem with generating file');
        }
    }
}
