<?php

namespace App\Http\Controllers;

/**
 * Class CodesGenerator
 * @package App\Http\Controllers
 */
class CodesGenerator
{
    /**
     * Generates random and unique codes by given quantity and length
     *
     * @param CodeContainer $container
     * @throws \Exception
     */
    public static function generate(CodeContainer $container)
    {
        $generatedData = [];
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $expChar = str_split($characters);

        $k = 0;

        try{
            while ($k < $container->getQuantity()) {
                $code = '';

                for ($i = 0; $i < $container->getLength(); $i++) {
                    $code .= $expChar[rand(0, sizeof($expChar) - 1)];
                }

                if (!in_array($code, $generatedData)) {
                    $generatedData[] = $code;
                    $k++;
                }
            }

            $container->setData($generatedData);
        } catch (\Exception $e) {
            throw new \Exception('There was a problem with codes generation');
        }
    }
}
