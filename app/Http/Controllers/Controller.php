<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

/**
 * Class Controller
 * @package App\Http\Controllers
 */
class Controller extends BaseController
{
    /**
     * @var
     */
    protected $dirName;

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generator()
    {
        return view('generator-form');
    }

    /**
     * Handle creating codes package process.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function generate(Request $request)
    {
        //TODO: validation of input
        try {
            $codeContainer = new CodeContainer(
                intval($request->get('quantity')),
                intval($request->get('length'))
            );

            $this->dirName = $codeContainer->getQuantity() . '_' . $codeContainer->getLength();

            if (file_exists(public_path($this->dirName))) {
                return view('list', ['file' => $this->dirName]);
            }

            CodesGenerator::generate($codeContainer);
            FileGenerator::create($codeContainer);

            return view('list', ['file' => $this->dirName]);

        } catch (\Exception $e) {
            var_dump($e);
        }
    }

    /**
     * Download file action
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download(Request $request)
    {
        $fileName = $request->get('file');
        $fileDirectory = public_path($this->dirName . '/' . $fileName);
        $directory = scandir($fileDirectory);

        $fullFilePath = $fileDirectory . '/' . $directory[2];

        $headers = array(
            'Content-Type: application/csv',
            'Content-Disposition: attachment; filename=example.csv',
            'Pragma: no-cache'
        );

        return Response::download($fullFilePath, $fileName, $headers);
    }
}
