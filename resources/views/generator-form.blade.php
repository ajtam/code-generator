<div class="row">
    <div class="col-12">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Codes generator</h3>
                </div>
                <form method="POST" enctype="multipart/form-data" action="{{ route('generate') }}">
                    {{ csrf_field() }}
                    <div class="row card-body">
                        <div class="col-md-3 form-group">
                            <label>Codes quantity</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-map"></i></span>
                                </div>
                                <input value="" type="text" class="form-control float-right" name="quantity" id="quantity">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-6"></div>
                        <div class="col-md-3 form-group">
                            <label>Codes length</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-map"></i></span>
                                </div>
                                <input value="" type="text" class="form-control float-right" name="length" id="length">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="col-md-12">
                            <input type="submit" class="btn btn-success" value="Generate"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
