<div class="row">
    <div class="col-12">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                        <a href="{{ route('download', ['file' => $file]) }}">
                        <button type="submit" class="btn btn-default">{{$file}}!</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
